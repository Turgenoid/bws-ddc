<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://thegazer.ru
 * @since      1.0.0
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/admin
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class Bws_Ddc_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bws_Ddc_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bws_Ddc_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bws-ddc-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bws_Ddc_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bws_Ddc_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bws-ddc-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_menus() {

	    add_options_page(
	        'Dining Deals Core Settings',
            'DDC Settings',
            'manage_options',
            'bws-ddc-settings',
            array( $this, 'display_bws_ddc_settings' )
        );

    }

    function display_bws_ddc_settings() {

        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/bws-ddc-settings.php';

    }

    public function register_pages_settings() {

	    add_settings_section(
	        $this->plugin_name . '-pages',
            'Pages',
            '',
            'bws-ddc-settings'
        );

	    add_settings_field(
	        $this->plugin_name . '-sales-page',
            'Sales Page for Vendors',
            array( $this, 'vendor_sales_page_cb' ),
            'bws-ddc-settings',
            $this->plugin_name . '-pages',
            array( 'label_for' => $this->plugin_name . '-vendor-sales-page' )
        );
	    register_setting( $this->plugin_name, $this->plugin_name . '-vendor-sales-page-id' );

        add_settings_field(
            $this->plugin_name . '-vendor-application-page',
            'Vendor Application Page',
            array( $this, 'vendor_application_page_cb' ),
            'bws-ddc-settings',
            $this->plugin_name . '-pages',
            array( 'label_for' => $this->plugin_name . '-vendor-application-page' )
        );
        register_setting( $this->plugin_name, $this->plugin_name . '-vendor-application-page-id' );

        add_settings_field(
            $this->plugin_name . '-voucher-tracking-page',
            'Voucher Tracking Page',
            array( $this, 'voucher_tracking_page_cb' ),
            'bws-ddc-settings',
            $this->plugin_name . '-pages',
            array( 'label_for' => $this->plugin_name . '-voucher-tracking-page' )
        );
        register_setting( $this->plugin_name, $this->plugin_name . '-voucher-tracking-page-id' );

        add_settings_field(
            $this->plugin_name . '-vap-thank-you-page',
            'Thank You Page for Vendor Application Form',
            array( $this, 'vap_thank_you_page_cb' ),
            'bws-ddc-settings',
            $this->plugin_name . '-pages',
            array( 'label_for' => $this->plugin_name . '-vap-thank-you-page' )
        );
        register_setting( $this->plugin_name, $this->plugin_name . '-vap-thank-you-page-id' );

    }

    public function vendor_sales_page_cb() {

        $vendor_sales_page_id = get_option( $this->plugin_name . '-vendor-sales-page-id' );

        echo "<select name='" . $this->plugin_name . "-vendor-sales-page-id'>";
        echo "<option disabled>Choose a Sales Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $vendor_sales_page_id ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo 'This sets the page used to display the front end sales page. This page should contain the following shortcode. <strong>[bws_lra_vendor_get_started]</strong>.';

    }

    public function vendor_application_page_cb() {

        $vendor_application_page_id = get_option( $this->plugin_name . '-vendor-application-page-id' );

        echo "<select name='" . $this->plugin_name . "-vendor-application-page-id'>";
        echo "<option disabled>Choose a Vendor Application Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $vendor_application_page_id ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end Vendor Application Page. This page should contain the following shortcode. <strong>[bws_lra_vendor_application_form]</strong>.';

    }

    public function voucher_tracking_page_cb() {

        $voucher_tracking_page_id = get_option( $this->plugin_name . '-voucher-tracking-page-id' );

        echo "<select name='" . $this->plugin_name . "-voucher-tracking-page-id'>";
        echo "<option disabled>Choose a Vendor Tracking Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $voucher_tracking_page_id ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end voucher tracking page. This page should contain the following shortcode. <strong>[bws_ddc_voucher_tracking_page]</strong>.';

    }

    public function vap_thank_you_page_cb() {

        $vap_thank_you_page_id = get_option( $this->plugin_name . '-vap-thank-you-page-id' );

        echo "<select name='" . $this->plugin_name . "-vap-thank-you-page-id'>";
        echo "<option disabled>Choose a Thank You Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $vap_thank_you_page_id ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";

    }

    public function custom_taxonomy_location() {

        $labels = array(
            'name'                       => 'Locations',
            'singular_name'              => 'Location',
            'menu_name'                  => 'Locations',
            'all_items'                  => 'All Locations',
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'new_item_name'              => 'New Location Name',
            'add_new_item'               => 'Add New Location',
            'edit_item'                  => 'Edit Location',
            'update_item'                => 'Update Location',
            'separate_items_with_commas' => 'Separate Location with commas',
            'search_items'               => 'Search Locations',
            'add_or_remove_items'        => 'Add or remove Locations',
            'choose_from_most_used'      => 'Choose from the most used Locations',
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );

        if( is_admin() && ( strpos( $_SERVER['REQUEST_URI'], 'post-new.php' ) !== FALSE ||
                strpos( $_SERVER['REQUEST_URI'], 'post.php' ) !== FALSE ) && get_the_ID() != 2911 ) {
            $args['hierarchical'] = false;
        }

        register_taxonomy( 'location', 'product', $args );

    }

    public function custom_taxonomy_type() {

        $labels = array(
            'name'                       => 'Type',
            'singular_name'              => 'Type',
            'menu_name'                  => 'Types',
            'all_items'                  => 'All Types',
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'new_item_name'              => 'New Type Name',
            'add_new_item'               => 'Add New Type',
            'edit_item'                  => 'Edit Type',
            'update_item'                => 'Update Type',
            'separate_items_with_commas' => 'Separate Type with commas',
            'search_items'               => 'Search Types',
            'add_or_remove_items'        => 'Add or remove Types',
            'choose_from_most_used'      => 'Choose from the most used Types',
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => false,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );

        register_taxonomy( 'type', 'product', $args );

    }

    public function save_location_if_empty( $post_id ) {

	    $location_terms = wp_get_post_terms( $post_id, 'location' );

	    if ( count( $location_terms ) == 0 ) {

            $vendor_id = get_post_meta( $post_id, '_woo_vou_vendor_user', true );

            if( empty( $vendor_id ) )
                $vendor_id = get_post( $post_id )->post_author;

            $vendor_zip = xprofile_get_field_data( 3, $vendor_id );

            if ( empty( $vendor_zip ) )
                return;

            $term_id = term_exists( strval( $vendor_zip ), 'location' )['term_id'];
            $terms_array = array( intval( $term_id ) );
            wp_set_post_terms( $post_id, $terms_array, 'location' );

        }

    }

}
