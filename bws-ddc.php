<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://thegazer.ru
 * @since             1.0.0
 * @package           Bws_Ddc
 *
 * @wordpress-plugin
 * Plugin Name:       Dining Deals Core
 * Plugin URI:        https://diningdeals.club
 * Description:       Core functionality for Dining Deals.
 * Version:           1.0.0
 * Author:            Oleg Sokolov
 * Author URI:        https://thegazer.ru
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bws-ddc
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'BWS_DDC_VERSION', '1.1.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bws-ddc-activator.php
 */
function activate_bws_ddc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bws-ddc-activator.php';
	Bws_Ddc_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bws-ddc-deactivator.php
 */
function deactivate_bws_ddc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bws-ddc-deactivator.php';
	Bws_Ddc_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bws_ddc' );
register_deactivation_hook( __FILE__, 'deactivate_bws_ddc' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bws-ddc.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bws_ddc() {

	$plugin = new Bws_Ddc();
	$plugin->run();

}
run_bws_ddc();
