<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://thegazer.ru
 * @since      1.0.0
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class Bws_Ddc_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
