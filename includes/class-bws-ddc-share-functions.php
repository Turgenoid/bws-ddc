<?php
class Bws_Ddc_Share_Functions {

    /**
     * Returns an array of vouchers by user id
     *
     * @param $user_id
     * @return array
     */
    public static function get_user_vouchers( $user_id ) {
        global $woo_vou_voucher;

        // Get completed orders by $user_id
        $user_orders_posts = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => wc_get_order_types(),
            'post_status' => 'Completed',
        ) );

        // Get orders
        $orders = array();
        foreach( $user_orders_posts as $user_order_post  ) {
            $order      = wc_get_order( $user_order_post->ID );
            $orders[]   = $order;
        }

        // Get vouchers
        $vouchers = array();
        foreach ( $orders as $order ) {
            foreach ( $order->get_downloadable_items() as $item ) {

                if ( get_post_meta( $item['product_id'], '_woo_vou_enable', true ) == 'yes' ) {

                    $voucher = array();

                    $order_id = $order->get_id();

                    $purchased_codes = $woo_vou_voucher->woo_vou_get_purchased_codes_by_product_id( $item['product_id'] );

                    foreach ( $purchased_codes as $pc ) {
                        if ( $pc['order_id'] != $order_id ) {
                            continue;
                        }

                        $voucher['codes'] = explode( ', ', $pc['vou_codes'] );

                        $voucher['purchase_date'] = $pc['order_date'];
                    }

                    if ( count( $voucher['codes'] ) == 0 )
                        continue;

                    $voucher['product_id'] = $item['product_id'];

                    $vendor_id = get_post_meta( get_post( $item['product_id'] )->ID, '_woo_vou_vendor_user', true );

                    if( empty( $vendor_id ) )
                        $vendor_id = get_post( $item['product_id'] )->post_author;

                    $voucher['vendor_address'] = get_user_meta( $vendor_id, '_woo_vou_address_phone', true );
                    $voucher['store'] = get_user_meta( $vendor_id, 'pv_shop_name', true );

                    $voucher['download_url'] = $item['download_url'];
                    $voucher['product_name'] = $item['product_name'];

                    $voucher_id = $item['download_id'][12];
                    $voucher['code'] = $voucher['codes'][$voucher_id-1];
                    $voucher['status'] = $woo_vou_voucher->woo_vou_get_voucher_code_status( $voucher['code'] );

                    $expiration_date = get_post_meta( $item['product_id'], '_woo_vou_exp_date', true );
                    $expiration_date = empty( $expiration_date ) ? 'Never' : $expiration_date;
                    $voucher['expiration_date'] = $expiration_date;

                    unset( $voucher['codes'] );

                    $vouchers[] = $voucher;

                }

            }

        }

        return $vouchers;

    }

    /**
     * Add zeros to the left Zip-Code side
     *
     * @param $zip_code
     * @return string
     */
    public static function zip_code_add_zeros( $zip_code ) {

        $zip_code = strval( $zip_code );
        $zero_count = 5 - strlen( $zip_code );
        $zeros = "";

        for ( $digit_number = 0; $digit_number < $zero_count; $digit_number++ ) {

            $zeros .= "0";

        }

        $zip_code = $zeros . $zip_code;

        return $zip_code;

    }

    /**
     * Get an array of a location for the specific zip code
     *
     * @param $zip
     * @return array|null
     */
    public static function get_city_state_by_zip( $zip ) {

        $csv = file_get_contents( plugin_dir_url(__FILE__) . 'res/postal_codes.csv' );
        $rows = explode( "\n", $csv );

        $location = null;

        foreach ( $rows as $row ) {

            $csv_row = str_getcsv( $row, ';' );

            if ( self::zip_code_add_zeros( $csv_row[0] ) == self::zip_code_add_zeros( $zip ) ) {
                $location = array(
                    'zip' => self::zip_code_add_zeros($zip),
                    'city' => $csv_row[1],
                    'state' => $csv_row[2]
                );

                break;

            }

        }

        return $location;

    }

    /**
     * Add new terms to the taxonomy of location
     *
     * @param $location
     * @return mixed
     */
    public static function add_new_terms_to_location_taxonomy( $location ) {

        // State
        if ( empty( $state_term = term_exists( $location['state'], 'location' ) ) )
            $state_term = wp_insert_term( $location['state'], 'location' );
        $state_term_id = $state_term['term_id'];

        // City
        if ( empty( $city_term = term_exists( $location['city'], 'location', $state_term_id ) ) )
            $city_term = wp_insert_term( $location['city'], 'location', $state_term_id );
        $city_term_id = $city_term['term_id'];

        // Zip Code
        if ( empty( $zip_term = term_exists( $location['zip'], 'location', $city_term_id ) ) )
            $zip_term = wp_insert_term( $location['zip'], 'location', $city_term_id );
        $zip_term_id = $zip_term['term_id'];

        return $zip_term_id;

    }

}