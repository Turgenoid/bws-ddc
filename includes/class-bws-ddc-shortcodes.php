<?php
/**
 * The shortcodes class.
 *
 * All plugins shortcodes are defined on this class
 *
 * @since      1.0.0
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */

class Bws_Ddc_Shortcodes {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->register_shortcodes();
    }

    /**
     * Register all plugin shortcodes
     */
    public function register_shortcodes() {

        // Voucher Tracking Page
        add_shortcode( 'bws_ddc_voucher_tracking_page', array( $this, 'voucher_tracking_page' ) );

        // Restaurant List Page
        add_shortcode( 'bws_ddc_restaurant_list_page', array( $this, 'restaurant_list_page' ) );

    }

    /**
     * Display the Voucher Tracking Page
     *
     * @return false|string
     */
    function voucher_tracking_page() {

        ob_start();

        do_action( 'bws_ddc/voucher_tracking_page' );

        $html = ob_get_contents();
        ob_end_clean();

        return $html;

    }


    /**
     * Display the Restaurant List Page
     *
     * @return false|string
     */
    function restaurant_list_page() {

        ob_start();

        do_action( 'bws_ddc/restaurant_list_page' );

        $html = ob_get_contents();
        ob_end_clean();

        return $html;

    }

}
