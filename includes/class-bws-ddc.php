<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://thegazer.ru
 * @since      1.0.0
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class Bws_Ddc {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Bws_Ddc_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'BWS_DDC_VERSION' ) ) {
			$this->version = BWS_DDC_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'bws-ddc';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Bws_Ddc_Loader. Orchestrates the hooks of the plugin.
	 * - Bws_Ddc_i18n. Defines internationalization functionality.
	 * - Bws_Ddc_Admin. Defines all hooks for the admin area.
	 * - Bws_Ddc_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-ddc-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-ddc-i18n.php';

        /**
         * The class responsible for defining shortcodes of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-ddc-shortcodes.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-bws-ddc-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-bws-ddc-public.php';

        /**
         * The class responsible for defining all support functions that occur in the admin area and public-facing.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-ddc-share-functions.php';

		$this->loader = new Bws_Ddc_Loader();
		$this->shortcodes = new Bws_Ddc_Shortcodes( $this->plugin_name, $this->get_version() );

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Bws_Ddc_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Bws_Ddc_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Bws_Ddc_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Menus
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menus' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'register_pages_settings' );

        // WooCommerce
        //$this->loader->add_action( 'init', $plugin_admin, 'custom_taxonomy_location' );
        $this->loader->add_action( 'init', $plugin_admin, 'custom_taxonomy_type' );
        //$this->loader->add_action( 'save_post_product', $plugin_admin, 'save_location_if_empty' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Bws_Ddc_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		// Custom Pages
        $this->loader->add_action( 'bws_ddc/voucher_tracking_page', $plugin_public, 'display_voucher_tracking_page' );
        $this->loader->add_filter( 'body_class', $plugin_public, 'add_class_for_body_voucher_tracking_page' );
        $this->loader->add_action( 'bws_ddc/restaurant_list_page', $plugin_public, 'display_restaurant_list_page' );

        // Modifications for WooCommerce PDF Vouchers
        $this->loader->add_action( 'woo_vou_check_qrcode_top', $plugin_public, 'add_redeem_btn_on_top' );
        $this->loader->add_filter( 'woo_vou_unlimited_voucher_code', $plugin_public, 'custom_unlimited_vou_code', 10, 2 );
            // Shortcodes
        $this->loader->add_filter( 'woo_vou_pdf_template_inner_html', $plugin_public, 'vou_shopname_shortcode', 10, 6 );
        $this->loader->add_filter( 'woo_vou_pdf_template_inner_html', $plugin_public, 'vou_shopaddress_shortcode', 10, 6 );

        // WooCommerce & WC Vendors Marketplace
        $this->loader->add_filter( 'wcv_seo_description_length', $plugin_public, 'zero_description_length' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Bws_Ddc_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
