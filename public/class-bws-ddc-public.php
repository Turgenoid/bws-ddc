<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://thegazer.ru
 * @since      1.0.0
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Bws_Ddc
 * @subpackage Bws_Ddc/public
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class Bws_Ddc_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bws_Ddc_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bws_Ddc_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bws-ddc-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bws_Ddc_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bws_Ddc_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bws-ddc-public.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Display Voucher Tracking Page
     */
	public function display_voucher_tracking_page() {

	    $voucher_tracking_page_id = get_option( $this->plugin_name . '-voucher-tracking-page-id' );

        if( get_the_ID() != $voucher_tracking_page_id  ) {
            return;
        }

        if ( !is_user_logged_in() ) {
            wp_redirect( wp_login_url() );
        }

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/bws-ddc-voucher-tracking-page.php' );
    }

    /**
     * Display Restaurant List Page
     */
    public function display_restaurant_list_page() {
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/bws-ddc-restaurant-list-page.php' );
    }

    /**
     * Add the 'Redeem' button on top of the Check Voucher Page
     */
    public function add_redeem_btn_on_top() {

        echo '<tr class="woo-vou-voucher-code-submit-wrap">
  			<td>
  				<input type="submit" id="woo_vou_voucher_code_submit" name="woo_vou_voucher_code_submit" class="button-primary" value="'.__( "Redeem", "woovoucher" ).'"/>
  				<div class="woo-vou-loader woo-vou-voucher-code-submit-loader"><img src="' . WOO_VOU_IMG_URL . '/ajax-loader.gif"/></div>
  			</td>			
  		</tr>';

    }

    /**
     * Replace the {shop_name} shortcode in vouchers
     *
     * @param $voucher_template_html
     * @param $orderid
     * @param $item_key
     * @param $items
     * @param $voucodes
     * @param $product_id
     * @return mixed
     */
    public function vou_shopname_shortcode( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $product_id ) {

        $vendor_id = get_post_meta( $product_id, '_woo_vou_vendor_user', true );
        if( empty($vendor_id) ) {
            $post = get_post( $product_id );
            $vendor_id = $post->post_author;
        }

        $shop_name = get_user_meta( $vendor_id, 'pv_shop_name', true );

        $voucher_template_html = str_replace( '{shopname}', $shop_name, $voucher_template_html );

        return $voucher_template_html;

    }

    /**
     * Replace the {locationvendoraddress} shortcode in vouchers
     *
     * @param $voucher_template_html
     * @param $orderid
     * @param $item_key
     * @param $items
     * @param $voucodes
     * @param $product_id
     * @return mixed
     */
    public function vou_shopaddress_shortcode( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $product_id ) {

        $vendor_id = get_post_meta( $product_id, '_woo_vou_vendor_user', true );
        if( empty($vendor_id) ) {
            $post = get_post( $product_id );
            $vendor_id = $post->post_author;
        }

        $state = get_user_meta( $vendor_id, 'vap_state', true );
        $city = get_user_meta( $vendor_id, 'vap_city', true );
        $zip = get_user_meta( $vendor_id, 'vap_zip', true );
        $location_str = $city . ', ' . $state . ', ' . $zip;
        $address = get_user_meta( $vendor_id, '_woo_vou_address_phone', true );
        $final_str = $location_str . ', ' . $address;

        $voucher_template_html = str_replace( '{locationvendoraddress}', $final_str, $voucher_template_html );

        return $voucher_template_html;

    }

    /**
     * Custom voucher codes for coupons
     *
     * @param $voucode
     * @param $args
     * @return string
     */
    public function custom_unlimited_vou_code( $voucode, $args ) {

        $user_id = get_post_meta( $args['order_id'], '_customer_user', true );
        $vendor_id = get_post_meta( $args['data_id'], '_woo_vou_vendor_user', true );

        if( empty( $vendor_id ) )
            $vendor_id = get_post( $args['product_id'] )->post_author;

        $shop_name = get_user_meta( $vendor_id, 'pv_shop_name', true );
        $shop_name = str_replace( ' ', '', $shop_name );
        $shop_name = mb_strtolower( $shop_name );

        $voucode = get_user_by( 'id', $user_id )->user_login[0] . $args['order_id'] . '-' . $shop_name;

        return $voucode;

    }

    /**
     * Fix issue with product description and shop header
     *
     * @param $length
     * @return int
     */
    public function zero_description_length( $length ) {

        return 0;

    }

    /**
     * Body class for Voucher Tracking Page
     *
     * @param $classes
     * @return array
     */
    public function add_class_for_body_voucher_tracking_page( $classes ) {

        $voucher_tracking_page_id = get_option( $this->plugin_name . '-voucher-tracking-page-id' );
        if ( get_the_ID() == $voucher_tracking_page_id ) {
            $classes[] = 'voucher-tracking-page';
        }

        return $classes;

    }

}
