<?php
$states = get_option( 'bws_ddc_states', true );

// Show a list of the states
echo "<div class='states-list'>";
foreach ( $states as $state ) {
    echo "<a href='#$state'>$state</a>";
}
echo "</div>";

// Show sections for the states
foreach ( $states as $state ) {
    // Get vendors for the current state
    $gu_args = array(
        'role'                  =>  'vendor',
        'meta_key'              =>  'vap_state',
        'meta_value'            =>  $state,
        'has_published_posts'   =>  true
    );
    $vendors_by_state = get_users( $gu_args );
    $vap_permalink = get_permalink( get_option( 'bws-ddc-vendor-application-page-id' ) );

    echo "<div class='state-section' id='$state'>";
    echo "<p class='state-name'>$state</p>";
    echo "<p>Don't see your city or your favorite restaurant on the list? <a hrev='$vap_permalink'>Add it here</a>.</p>";
    echo "<div class='state-vendors'>";

    // Get all cities by this state and connect with vendors
    $vbc = array();
    foreach ($vendors_by_state as $key => $vendor ) {
        // Filter Businesses by Type
        $business_type = get_user_meta( $vendor->ID, 'vap_business_type', true );
        if ( $business_type != 'Restaurant' )
            continue;

        $city = get_user_meta( $vendor->ID, 'vap_city', true );
        if ( !in_array( $city, $vbc ) )
            $vbc[$city][] = $vendor->ID;
    }

    foreach ( $vbc as $city => $vendors ) {

        echo "<p class='city-name'>$city</p>";

        foreach ($vendors as $vendor_id) {
            echo "<div class='vendor-wrapper'>";

            $logo = get_user_meta($vendor_id, '_woo_vou_logo', true);
            $business_name = get_user_meta($vendor_id, 'pv_shop_name', true);
            $p_args = array(
                'post_type'     => 'product',
                'post_status'   => 'publish',
                'numberposts'   =>  -1,
                'author'   =>  $vendor_id
            );
            $products = get_posts($p_args);

            echo "<div class='vendor-title'><p>$business_name</p><div class='vendor-logo-container'><img src='$logo'></div></div>";

            echo "<ul class='products-list'>";
            foreach ($products as $product) {
                $link = get_permalink($product->ID);
                $title = $product->post_title;

                echo "<li><a href='$link' target='_blank'>$title</a></li>";
            }
            echo "</ul>";

            echo "</div>";
        }

    }

    echo "</div>";
    echo "</div>";
}